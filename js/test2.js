const test2 = async (gl, video) => {

  const stream = await navigator.mediaDevices.getUserMedia({ video: true });
  await new Promise((resolve) => {
    video.srcObject = stream;
    video.onplay = resolve();
  })

  const program = await getProgram(
    'shaders/test2V.glsl',
    'shaders/test2F.glsl'
  );

  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

  // attributes receive and interpret data from buffer
  const positionAttributeLocation = 
    gl.getAttribLocation(program, "a_position");
  const positionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
    -1.0, -1.0,
    1.0,  -1.0,
    -1.0,  1.0,
    -1.0,  1.0,
    1.0,  -1.0,
    1.0,  1.0]), gl.STATIC_DRAW);
  gl.enableVertexAttribArray(positionAttributeLocation);
  gl.vertexAttribPointer(positionAttributeLocation, 2, gl.FLOAT, false, 0, 0);

  const texCoordLocation = gl.getAttribLocation(program, "a_texCoord");
  const texBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, texBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
    -1.0, -1.0,
    1.0,  -1.0,
    -1.0,  1.0,
    -1.0,  1.0,
    1.0,  -1.0,
    1.0,  1.0]), gl.STATIC_DRAW);
  gl.enableVertexAttribArray(texCoordLocation);
  gl.vertexAttribPointer(texCoordLocation, 2, gl.FLOAT, false, 0, 0);

  const keyColorUniformLocation = 
  gl.getUniformLocation(program, "u_keyColor");

  const thresholdUniformLocation = 
  gl.getUniformLocation(program, "u_threshold");

  // Create a texture.
  const texture = gl.createTexture();

  // Tell it to use our program (pair of shaders)
  gl.useProgram(program);

  gl.enableVertexAttribArray(positionAttributeLocation);
  gl.enableVertexAttribArray(texCoordLocation);

  // you must bind positionBuffer to ARRAY_BUFFER before
  // calling g.vertexAttribPointer
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
  let size = 2;          // 2 components per iteration
  let type = gl.FLOAT;   // the data is 32bit floats
  let normalize = false; // don't normalize the data
  let stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
  let offset = 0;        // start at the beginning of the buffer
  gl.vertexAttribPointer(
    positionAttributeLocation, size, type, normalize, stride, offset
  );

  gl.bindBuffer(gl.ARRAY_BUFFER, texBuffer);
  // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
  size = 2;                // 2 components per iteration
  type = gl.FLOAT;
  normalize = false;
  stride = 0;              // 0 = move forward size * sizeof(type) each iteration to get the next position
  offset = 0;              // start at the beginning of the buffer
  gl.vertexAttribPointer(
    texCoordLocation, size, type, normalize, stride, offset
  );

  const rgb = {
    r: 0,
    g: 0,
    b: 0,
    a: 1,
  };

  let threshold = .1;

  const sliderBar = document.createElement('input');
  sliderBar.type = 'range';
  sliderBar.max = .5;
  sliderBar.min = .0;
  sliderBar.step = .01;
  sliderBar.value = .1;
  document.body.appendChild(sliderBar);

  sliderBar.oninput = (e) => {
    const val = e.target.value;
    threshold = val;
  }

  video.addEventListener('click', (e) => {
    let bounds = video.getBoundingClientRect();
    let x = e.offsetX;
    let y = e.offsetY;

    let xPerc = x / bounds.width;
    let yPerc = y / bounds.height;

    x = video.videoWidth * xPerc;
    y = video.videoHeight * yPerc;

    const tempCan = document.createElement('canvas');
    tempCan.width = 1;
    tempCan.height = 1;
    const tempCtx = tempCan.getContext('2d');
    tempCtx.drawImage(video, -x, -y);
    const imageData = tempCtx.getImageData(0, 0, 1, 1);
    const uint8 = imageData.data;;
    rgb.r = uint8[0] / 255;
    rgb.g = uint8[1] / 255;
    rgb.b = uint8[2] / 255;

    document.body.style.backgroundColor = `rgb(${rgb.r * 255}, ${rgb.g * 255}, ${rgb.b * 255})`;

  });

  setInterval(() => {
    gl.bindTexture(gl.TEXTURE_2D, texture);
  
    // Set the parameters so we can render any size image.
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    
    // Upload the image into the texture.
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, video);

    gl.uniform4f(keyColorUniformLocation, rgb.r, rgb.g, rgb.b, rgb.a);
    gl.uniform1f(thresholdUniformLocation, threshold);

    let primitiveType = gl.TRIANGLES;
    let first = 0;
    let count = 6;
    gl.drawArrays(primitiveType, first, count);

  }, 30);

}
