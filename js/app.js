const can = document.querySelector('#canvas');
const gl = can.getContext('webgl');
resizeCanvasToDisplaySize(can);

const video = document.querySelector('#video');

const buttonRow = document.querySelector('.buttonRow');

let tests = [
  { name: 'test1', func: () => test1(gl) },
  { name: 'test2', func: () => test2(gl, video) }, 
];

tests.forEach((test) => {
  let button = document.createElement('button');
  button.onclick = test.func;
  button.innerText = test.name;
  buttonRow.appendChild(button);
});