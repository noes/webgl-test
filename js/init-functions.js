// loads shader file
const loadShaderFile = async (name) => {
  let shader = await fetch(name);
  shader = await shader.text();
  return shader;
};

// creates shader
const createShader = (gl, type, source) => {
  var shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (success) {
    return shader;
  }

  console.log(gl.getShaderInfoLog(shader));
  gl.deleteShader(shader);
}

function createProgram(gl, vertexShader, fragmentShader) {
  var program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);
  var success = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (success) {
    return program;
  }
 
  console.log(gl.getProgramInfoLog(program));
  gl.deleteProgram(program);
}

// initializes the shaders and the program
const getProgram = async (vertName, fragName) => {
  const vertFile = await loadShaderFile(vertName);
  const fragFile = await loadShaderFile(fragName);

  const vertShader = createShader(gl, gl.VERTEX_SHADER, vertFile);
  const fragShader = createShader(gl, gl.FRAGMENT_SHADER, fragFile);

  const program = createProgram(gl, vertShader, fragShader);
  return program;
}

const resizeCanvasToDisplaySize = (canvas) => {
  // Lookup the size the browser is displaying the canvas in CSS pixels.
  const displayWidth  = canvas.clientWidth;
  const displayHeight = canvas.clientHeight;
 
  // Check if the canvas is not the same size.
  const needResize = canvas.width  !== displayWidth ||
                     canvas.height !== displayHeight;
 
  if (needResize) {
    // Make the canvas the same size
    canvas.width  = displayWidth;
    canvas.height = displayHeight;
  }
 
  return needResize;
}