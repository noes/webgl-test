const test1 = async (gl) => {
  const program = await getProgram(
    'shaders/test1V.glsl',
    'shaders/test1F.glsl'
  );

  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

  // attributes receive and interpret data from buffer
  const positionAttributeLocation = 
    gl.getAttribLocation(program, "a_position");
  const positionBuffer = gl.createBuffer();

  // grab u_resolution global so we can assign it a value
  const colorAttributeLocation = 
    gl.getAttribLocation(program, "a_color");
  const colorBuffer = gl.createBuffer();

  // grab u_resolution global so we can assign it a value
  const resolutionUniformLocation = 
    gl.getUniformLocation(program, "u_resolution");

  // grab u_resolution global so we can assign it a value
  const colorUniformLocation = 
    gl.getUniformLocation(program, "u_color");

  // Tell it to use our program (pair of shaders)
  gl.useProgram(program);

  gl.enableVertexAttribArray(positionAttributeLocation);
  gl.enableVertexAttribArray(colorAttributeLocation);

  // this will tell our vertex shader the width and height of the canvas
  gl.uniform2f(resolutionUniformLocation, gl.canvas.width, gl.canvas.height);

  // you must bind positionBuffer to ARRAY_BUFFEr before
  // calling g.vertexAttribPointer
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
  let size = 2;          // 2 components per iteration
  let type = gl.FLOAT;   // the data is 32bit floats
  let normalize = false; // don't normalize the data
  let stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
  let offset = 0;        // start at the beginning of the buffer
  gl.vertexAttribPointer(
    positionAttributeLocation, size, type, normalize, stride, offset
  );

  gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
  // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
  size = 3;                // 3 components per iteration
  type = gl.UNSIGNED_BYTE;
  normalize = true;
  stride = 0;              // 0 = move forward size * sizeof(type) each iteration to get the next position
  offset = 0;              // start at the beginning of the buffer
  gl.vertexAttribPointer(
    colorAttributeLocation, size, type, normalize, stride, offset
  );

  canvas.addEventListener('mousemove', (e) => {
    let x = e.offsetX;
    let y = e.offsetY;

    let r = x / gl.canvas.width;
    let g = y / gl.canvas.height;
    // this will tell our vertex shader the width and height of the canvas
    gl.uniform4f(colorUniformLocation, r, g, 1, 1);

    // Clear the canvas
    gl.clearColor(0, 0, 0, 0);
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    const positions = new Float32Array([
      0, 0,
      0, 100,
      100, 0,
      100, 0,
      x, y,
      0, 100,
      400, 300,
      400, 200,
      x, y,
      x, y,
      300, 300,
      400, 300,
    ]);
    gl.bufferData(gl.ARRAY_BUFFER, positions, gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    const rgbs = new Uint8Array([
      0, 0, 0,
      0, 0, 0,
      0, 0, 0,
      0, 0, 255,
      0, 0, 255,
      0, 0, 255,
      0, 255, 0,
      0, 255, 0,
      0, 255, 0,
      0, 255, 255,
      0, 255, 255,
      0, 255, 255,
    ]);
    gl.bufferData(gl.ARRAY_BUFFER, rgbs, gl.STATIC_DRAW);

    let primitiveType = gl.TRIANGLES;
    let first = 0;
    let count = 12;
    gl.drawArrays(primitiveType, first, count);

  });
}
