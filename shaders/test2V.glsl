attribute vec4 a_position;
attribute vec2 a_texCoord;
varying vec2 v_texCoord;
varying vec4 v_position;
 
void main() {
 
  v_texCoord = a_texCoord;
  v_position = a_position;
  gl_Position = a_position;

}