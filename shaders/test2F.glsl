precision mediump float;
 
// our texture
uniform sampler2D u_image;
uniform vec4 u_keyColor;
uniform float u_threshold;
// the texCoords passed in from the vertex shader.
varying vec2 v_texCoord;
varying vec4 v_position;
 
void main() {
   // Look up a color from the texture.

  vec2 adj = v_texCoord / .5;

  vec4 rgb = texture2D(u_image, (vec2(v_position.x, -v_position.y) / 2.0) + .5);

  if (length(rgb - u_keyColor) < u_threshold) {
    rgb.a = 0.0;
  }

  gl_FragColor = rgb;
}