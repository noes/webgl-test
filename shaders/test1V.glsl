// an attribute will receive data from a buffer
attribute vec2 a_position;
attribute vec3 a_color;
uniform vec2 u_resolution;
varying vec4 v_color;
 
// all shaders have a main function
void main() {
 
  // convert from pixel coords to range (0.0 -> 1.0)
  vec2 zeroToOne = a_position / u_resolution;
  // convert 0->1 to 0->2
  vec2 zeroToTwo = zeroToOne * 2.0;
  // convert to -1 -> 1
  vec2 clipSpace = (zeroToTwo - 1.0) * vec2(1, -1);
  // gl_Position is a special variable a vertex shader
  // is responsible for setting
  gl_Position = vec4(clipSpace, 0, 1);
  v_color = vec4(a_color, 1);

}